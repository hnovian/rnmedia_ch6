import React, { useEffect, useState } from 'react';
import {  
  StyleSheet,
  View,
  Button
} from 'react-native';
import { GoogleSignin } from '@react-native-google-signin/google-signin';
import auth from '@react-native-firebase/auth';
import { LoginManager, AccessToken } from 'react-native-fbsdk-next';
import TouchID from 'react-native-touch-id';

export default function LoginScreen() {

  useEffect(() => {
    GoogleSignin.configure({
      webClientId: '604610131645-ia06b7l9a8v4docl1jdbcb9iphgm8jep.apps.googleusercontent.com',
    });
  });

  function onFingerPrintPress() {
    const optionalConfigObject = {
      title: 'Authentication Required', // Android
      imageColor: '#e00606', // Android
      imageErrorColor: '#ff0000', // Android
      sensorDescription: 'Touch sensor', // Android
      sensorErrorDescription: 'Failed', // Android
      cancelText: 'Cancel', // Android
      fallbackLabel: 'Show Passcode', // iOS (if empty, then label is hidden)
      unifiedErrors: false, // use unified error messages (default false)
      passcodeFallback: false, // iOS - allows the device to fall back to using the passcode, if faceid/touch is not available. this does not mean that if touchid/faceid fails the first few times it will revert to passcode, rather that if the former are not enrolled, then it will use the passcode.
    };

    TouchID.authenticate('to demo this react-native component', optionalConfigObject)
      .then(success => {
        console.log(success)
        alert('Authenticated Successfully, Signed in with Fingerprint!');
      })
      .catch(error => {
        console.log(error)
        alert('Authentication Failed');
      });
  }

  async function onGoogleButtonPress() {
    // Get the users ID token
    const { idToken } = await GoogleSignin.signIn();
  
    // Create a Google credential with the token
    const googleCredential = auth.GoogleAuthProvider.credential(idToken);
  
    // Sign-in the user with the credential
    return auth().signInWithCredential(googleCredential);
  }

  async function onFacebookButtonPress() {
    console.log("facebook login")
    // Attempt login with permissions
    const result = await LoginManager.logInWithPermissions(['public_profile', 'email']);
  
    if (result.isCancelled) {
      alert("User cancelled the login process")
      throw 'User cancelled the login process';
    }
  
    // Once signed in, get the users AccesToken
    const data = await AccessToken.getCurrentAccessToken();
  
    if (!data) {
      alert("Something went wrong obtaining access token")
      throw 'Something went wrong obtaining access token';
    }
  
    // Create a Firebase credential with the AccessToken
    const facebookCredential = auth.FacebookAuthProvider.credential(data.accessToken);
    console.log(facebookCredential)
  
    // Sign-in the user with the credential
    return auth().signInWithCredential(facebookCredential);
  }

  return (
    <View style={styles.container}>
        <Button
            title="Google Sign-In"
            onPress={() => onGoogleButtonPress().then(() => alert('Signed in with Google!'))}
        />
        <Button
            title="Facebook Sign-In"
            onPress={() => onFacebookButtonPress().then(() => alert('Signed in with Facebook!'))}
        />
         <Button
            title="Fingerprint"
            onPress={() => onFingerPrintPress()}
        />
    </View>
  );
}

//create our styling code:
const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignContent: 'center',
    alignItems: 'center',
    flexDirection: 'column'
  },
});